<?php
    session_start();
    require_once('database.php');

    $view_query = "select * from list";
    $information = mysqli_query($connection,$view_query);

    if(!isset($_SESSION['login'])){
        header('location: login.php');
    }

?>

<h1> ALL TASK LIST </h1>

    <form style="float:left" action="logout.php" method="post" >
      <button  class="btn btn-warning" type="submit" name = "logout" value = "one">LOGOUT</button>
    </form>

    <form style="float:left" action="delete_all.php" >
      <button  class="btn btn-danger" type="submit" name = "delete" value = "one">DELETE ALL</button>
    </form>

    <form style="float:left" action="delete_all_done.php" >
      <button  class="btn btn-danger" type="submit" name = "delete" value = "one">DELETE ALL DONE</button>
    </form>

    <form style="float:left" action="index.php" >
      <button  class="btn btn-info" >ADD TO DO</button>
    </form>

        <?php
            echo date("d-m-Y");
        ?>

  <table class="table table-striped table-dark table-bordered">
      <thead>
      <tr>
          <th scope="col">NO.</th>
          <th scope="col">TASK LIST</th>
          <th scope="col">DATE</th>
          <th scope="col">ACTION</th>
      </tr>
      </thead>

      <tbody>

      <?php
      $counter = 1;
      foreach($information as $single_data) { ?>
      <tr>
          <td><?=  $counter ?> </td>
          <td><?=  $single_data['task'] ?> </td>
          <td><?=  $single_data['date'] ?> </td>
          <td>
              <?php if($single_data['status'] == 1){?>
              <a href="done.php?list_id=<?= $single_data['id'] ?>"> DONE </a>
              |
          <?php }

                else{
                    echo "ALREADY DONE |";
                }

          ?>

               <a href="delete.php?list_id= <?= $single_data['id'] ?>" > DELETE </a> </td>
      </tr>
      <?php
            $counter++;
        } ?>
      </tbody>
  </table>
