<?php
    session_start();
    require_once("database.php");

    if(!isset($_SESSION['login'])){
        header('location: login.php');
    }
    
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>TO DO</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1>TO DO LIST</h1>
                    <br/>

                    <?php
                        echo "TODAY IS ".date("d-m-Y");
                    ?>

                    <form style="float:right" action="list_view.php" >
                      <button  class="btn btn-info" >TASK LIST VIEW</button>
                    </form>

                    <br/>
                    <br/>
                    <form action="list.php" method="post">
                        <div class="form-group">
                            <label for="">TASK</label>
                            <input class="form-control" type="hidden" name="date" value="<?= date("d-m-Y") ?>"><br>
                            <input class="form-control" type="text" name="task">
                        </div>
                        <div class="form-group">
                            <button class="form-control btn btn-primary" type="submit">ADD TASK</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
